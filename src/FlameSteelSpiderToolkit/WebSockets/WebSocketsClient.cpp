#include <FlameSteelCore/Utils.h>
#include <FlameSteelSpiderToolkit/WebSockets/WebSocketsClient.h>
#include <FlameSteelSpiderToolkit/WebSockets/easywsclient/easywsclient.hpp>
#include <FlameSteelSpiderToolkit/ClientDelegate.h>
#include <iostream>

using namespace FlameSteelCore::Utils;
using easywsclient::WebSocket;


WebSocketsClient::WebSocketsClient(string address) {
	this->address = address;
	cout << address << endl;
};

void WebSocketsClient::connect(shared_ptr<ClientDelegate> delegate) {

    socket = WebSocket::from_url(address);
	if (socket == nullptr) {
		string errorString = "Can't connect to websockets address: ";
		errorString += address;
		throwRuntimeException(errorString);
	}

	this->delegate = delegate;

};

void WebSocketsClient::step() {
	if (socket == nullptr) {
		string errorString = "Can't poll websockets, sockets = nullptr ";
		throwRuntimeException(errorString);
	}
	WebSocket::pointer ws = (WebSocket::pointer) socket;
	ws->poll();
	ws->dispatch([this, ws](const string & data) {
				cout << ws << endl;
				cout << this << endl;
				cout << "Response data " << data << endl;

				if (this->delegate.get() == nullptr) {
					string errorString = "Can't handle input websockets data, because delegate is nullptr";
					throwRuntimeException(errorString);
				}
				this->delegate->clientDidReceiveData(shared_from_this(), data);

			});
};

void WebSocketsClient::disconnect() {
};

void WebSocketsClient::send(string data) {
	if (socket == nullptr) {
		cout << "ws = nullptr" << endl;
		exit(1);
	}
	WebSocket::pointer ws = (WebSocket::pointer) socket;
	ws->send(data.c_str());
};
