#ifndef FLAMESTEELSPIDERTOOLKIT_WEBSOCKETS_CLIENT_DEFINED
#define FLAMESTEELSPIDERTOOLKIT_WEBSOCKETS_CLIENT_DEFINED

#include <FlameSteelSpiderToolkit/Client.h>

using namespace FlameSteelSpiderToolkit;

namespace FlameSteelSpiderToolkit {

class WebSocketsClient: public Client {

public:
	WebSocketsClient(string address);
	virtual void connect(shared_ptr<ClientDelegate> delegate);
	virtual void step();
	virtual void disconnect();
	virtual void send(string data);
	virtual ~WebSocketsClient() {};

private:
	void *socket;
	string address;

};

};

#endif
