#ifndef FLAMESTEELSPIDERTOOLKIT_CLIENT_DELEGATE_DEFINED
#define FLAMESTEELSPIDERTOOLKIT_CLIENT_DELEGATE_DEFINED

namespace FlameSteelSpiderToolkit {

class ClientDelegate {

public:
  virtual void clientDidReceiveData(shared_ptr<Client> client, string data) = 0;

};

};

#endif
