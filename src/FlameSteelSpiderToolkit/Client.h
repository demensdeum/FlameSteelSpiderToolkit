#ifndef FLAMESTEELSPIDERTOOLKIT_CLIENT_DEFINED
#define FLAMESTEELSPIDERTOOLKIT_CLIENT_DEFINED

#include <string>
#include <memory>

using namespace std;


namespace FlameSteelSpiderToolkit {

class ClientDelegate;

class Client: public enable_shared_from_this<Client> {

public:
  virtual void connect(shared_ptr<ClientDelegate> delegate) = 0;
  virtual void step() = 0;
  virtual void disconnect() = 0;
  virtual void send(string data) = 0;

  shared_ptr<ClientDelegate> delegate;

};

};

#endif
